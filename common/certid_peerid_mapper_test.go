/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package common

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var (
	certId   = "cert id"
	peerId   = "peer id"
	certId_2 = "cert id2"
	peerId_2 = "peer id2"
)

func TestCertIdPeerIdMapper_Add(t *testing.T) {
	certIdPeerIdMapper := NewCertIdPeerIdMapper(nil)
	require.Empty(t, certIdPeerIdMapper.mapper)
	certIdPeerIdMapper.Add(certId, peerId)
	require.Contains(t, certIdPeerIdMapper.mapper, certId)
}

func TestCertIdPeerIdMapper_RemoveByPeerId(t *testing.T) {
	certIdPeerIdMapper := NewCertIdPeerIdMapper(nil)
	certIdPeerIdMapper.Add(certId, peerId)
	require.Contains(t, certIdPeerIdMapper.mapper, certId)
	certIdPeerIdMapper.RemoveByPeerId(peerId)
	require.Empty(t, certIdPeerIdMapper.mapper)
}

func TestCertIdPeerIdMapper_GetAll(t *testing.T) {
	certIdPeerIdMapper := NewCertIdPeerIdMapper(nil)
	require.Empty(t, certIdPeerIdMapper.mapper)
	certIdPeerIdMapper.Add(certId, peerId)
	require.Contains(t, certIdPeerIdMapper.mapper, certId)
	list := certIdPeerIdMapper.GetAll()
	require.Equal(t, list[0], certId)
	require.Equal(t, list[1], peerId)
	require.Equal(t, len(list), 2)

	certIdPeerIdMapper.Add(certId_2, peerId_2)
	list = certIdPeerIdMapper.GetAll()
	require.Equal(t, list[2], certId_2)
	require.Equal(t, list[3], peerId_2)
	require.Equal(t, len(list), 4)
}
